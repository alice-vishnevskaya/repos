import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:incomplete_app/themes.dart';

class Settings extends StatefulWidget
{


  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool isBusy=false;
  Language lang = Language.Russian;

  
  String langToString(Language lang)
  {
    switch(lang)
    {
      case Language.English: return "English";
      //case Language.Russian: return "Русский";
    }
    return "English";
  }
  
  @override
  Widget build(BuildContext context) {
    return   new Scaffold(backgroundColor: Color(0xffEBEBEB),
      body:Column(mainAxisAlignment: MainAxisAlignment.center,
          children:[
      Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color:Color(0xff79B7F0)
        ),
        child:
        Text('Settings',
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20,
            color: Colors.white
          ),
        )),

        Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
     decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      color: Colors.white
      ),
      child:
      Container(margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child:
      Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
            Text('  Busy mode',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            Container(child:
            Row(children:
            [
              CupertinoSwitch(value: isBusy,
                trackColor :Color(0xFFFF6A6A),
                activeColor: Colors.blue,
                onChanged:(bool newValue) {
              setState(() {
                isBusy = newValue;
              });
            }
            ),
              Text((isBusy)?'On':'Off',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
            ),
            ),

          ],),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('  Language',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                ),
              ),
              new DropdownButton<Language>(
                value: lang,
                elevation: 16,
                underline: Container(
                  height: 0,
                  color: Colors.deepPurpleAccent,
                ),
                items: Language.values.map((Language lang) {
                  return new DropdownMenuItem<Language>(
                    value: lang,
                    child: new Text(langToString(lang)),
                  );
                }).toList(),
                onChanged: (Language value) {
                  setState(() {
                    lang = value;
                  });
                },
              )

            ],
          ),
          Container(alignment: Alignment.centerLeft,
              child: TextButton(onPressed: (){}, child:
              Text("Security",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 18,
              )))
          ),
          Container(alignment: Alignment.centerLeft,
              child: TextButton(onPressed: (){}, child:
              Text("About",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                  )))
          ),
        ],
      )
      )
    )
      ])
    );
  }
}

enum Language
{
  Russian,English
}