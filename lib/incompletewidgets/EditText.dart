import 'package:flutter/material.dart';

  Widget EditText(String title, {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal : 5 , vertical : 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 15),
          ),
          SizedBox(
            height: 5,
          ),
          TextField(
              obscureText: isPassword,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true)
            )
        ],
      ),
    );
  }