import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:incomplete_app/screen_bar.dart';
import 'package:incomplete_app/screen_register.dart';
import 'incompletewidgets/EditText.dart';
import 'incompletewidgets/IncompleteButton.dart';
import 'package:incomplete_app/incompletewidgets/move.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:incomplete_app/auth_service.dart';



class ScreenLogin extends StatefulWidget {
  @override
  _ScreenRegisterState createState() => _ScreenRegisterState();
}

List<Widget> children = [
                  
];

bool reg = true; 
bool regcount = false;

Widget temp;

class _ScreenRegisterState extends State<ScreenLogin> {

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  void logUserIn(){
    context.read<AuthenticationService>().signIn(
      emailController.text.trim(),
      passwordController.text.trim(),
    );
    
  }

  void regUser(){
    context.read<AuthenticationService>().signUp(
      emailController.text.trim(),
      passwordController.text.trim(),
    );
  }

  void oformitb(bool r) 
  { 
    setState((){reg = r;});
  }

  void oformitbb()
  {
    setState((){regcount = true;});
  }

  void getBack() 
  { 
    if (!reg) 
    setState(
      ()
      {
       //regSwap();
      }
    );
    reg = true;
  }

  @override
  Widget build(BuildContext context) {

    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
    //   appBar: AppBar(
    //   backgroundColor: Colors.white, // status bar color
    //   brightness: Brightness.dark, // status bar brightness
    // ),
      body: Container(
        height : height,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
        
          //crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Container(
              padding: EdgeInsets.only(right: width/13, left: width/13),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                
                children: <Widget>[

                  Text('meetem',
                    style: TextStyle(
                      fontSize: width/6,
                      fontWeight: FontWeight.w400  
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(horizontal : 5 , vertical : 10),
                    child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "E-mail adress",
                        style: TextStyle(fontSize: 15),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      TextField(
                          controller: emailController,
                          obscureText: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)
                        )
                      ],
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(horizontal : 5 , vertical : 10),
                    child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Password",
                        style: TextStyle(fontSize: 15),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      TextField(
                        controller: !reg ? null : passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)
                        )
                      ],
                    ),
                  ),

                  Container(
                    height: !reg ? null : 0,
                    child:
                    Container(
                    margin: EdgeInsets.symmetric(horizontal : 5 , vertical : 10),
                    child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Repeat password",
                        style: TextStyle(fontSize: 15),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      TextField(
                        controller: !reg ? passwordController : null,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)
                        )
                      ],
                    ),
                  ),
                  ),
                  
                  Align(
                    alignment: Alignment.topRight,
                    child:
                  Column(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                    Container(
                    height: reg ? null : 0,
                    margin: EdgeInsets.symmetric(horizontal : 5),
                    child: Text('Forgot the password?',
                      //textWidthBasis: TextWidthBasis.longestLine,
                      style: TextStyle(
                        //color: Color(0xfff3f3f4),
                        fontSize: 15, 
                      ),
                    ),
                    ),

                    SizedBox(
                        height: reg ? 10 : 0,
                    ),

                    ]
                  ),
                  ),

                  SizedBox(
                    height: 5,
                  ),

                  // RaisedButton(
                  //   child: Text('Register'), 
                  //   onPressed: null,            
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    //mainAxisSize: MainAxisSize.max,
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                      IncompleteButton('Sign in', size : reg ? width/13*11/3 : 0, 
                        todo: (){
                          //переход на экран с баром
                          //
                          //Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenBar()));
                          logUserIn();
                        } 
                      ),
                      ]
                      ),

                      Container( 
                      margin: EdgeInsets.symmetric(horizontal : 7 , vertical : 10),
                      child: InkWell(
                          
                        onTap: () {
                          oformitb(true);
                          //getBack();
                        },

                        child: Container(
                          width : !reg ? null : 0,
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(40),color: Color(0xff79B7F0)),
                        child: Ink(
                        //margin: EdgeInsets.symmetric(horizontal : 5 , vertical : 10),
                        padding: EdgeInsets.all(20),
                        child: Container(
                          child: Row(
                            
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,

                            children: <Widget>[

                              // Container(
                              //   padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
                              //   child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
                              // ),
                              
                              Icon( Icons.arrow_back , color: Colors.white,),

                            ],
                          ),
                        ),
                      ))
                      )
                      ),

                      SizedBox(width : 10),

                      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                      IncompleteButton('Sign up', size :  width/13*11/3,  
                      todo: (){
                        // Navigator.push(context,
                        // MaterialPageRoute(builder: (context) => ScreenRegister()),
                        // )
                        
                        oformitb(false);
                        if (regcount) regUser();
                        oformitbb();
                        // );
                        // setState(_ScreenRegisterState);
                        } 
                      ),
                      ]
                      ),
                    ]
                  ),
                ]
              )
            ),

          ],
      )
      ),
    ); 
  }
}

