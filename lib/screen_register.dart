import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './incompletewidgets/EditText.dart';
import './incompletewidgets/IncompleteButton.dart';

class ScreenRegister extends StatefulWidget {
  @override
  _ScreenRegisterState createState() => _ScreenRegisterState();
}

class _ScreenRegisterState extends State<ScreenRegister> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
    //   appBar: AppBar(
    //   backgroundColor: Colors.white, // status bar color
    //   brightness: Brightness.dark, // status bar brightness
    // ),
      body: Container(
        height : height,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
        
          //crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Container(
              padding: EdgeInsets.only(right: width/13, left: width/13),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                
                children: <Widget>[

                  Text('meetem',
                    style: TextStyle(
                      fontSize: width/5,
                      fontWeight: FontWeight.w400  
                    ),
                  ),

                  EditText('Username', ),

                  EditText('Password', isPassword: true),

                  EditText('Repeat password', isPassword: true),
                  
                  SizedBox(
                    height: 22,
                  ),

                  SizedBox(
                    height: 5,
                  ),

                  // RaisedButton(
                  //   child: Text('Register'), 
                  //   onPressed: null,            
                  // ),

                  IncompleteButton('Register')

                ]
              )
            ),

          ],
      )
      ),
    ); 
  }
}

