//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:incomplete_app/auth_service.dart';
import 'package:incomplete_app/screen_bar.dart';
import 'package:incomplete_app/screen_login.dart';
import 'package:incomplete_app/screen_register.dart';
import 'package:incomplete_app/screen_login.dart';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Root());
}

class Root extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      //statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.dark,
    ));

    return MultiProvider(
      providers: [
        Provider<AuthenticationService>(
          create: (_) => AuthenticationService(FirebaseAuth.instance),
          
        ),
        StreamProvider(
          create: (context) => context.read<AuthenticationService>().authStateChanges,
        )
      ],
      child: MaterialApp(
        title: 'meetem incomplete',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        //home: ScreenLogin(),
        //
        home: AuthenticationWrapper(),
      )
    );
  }
}

class AuthenticationWrapper extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    final firebaseuser = context.watch<User>();
    //return Container();
    if (firebaseuser != null){
      
      
      return ScreenBar();

    }
    return ScreenLogin();
  }
}

 

