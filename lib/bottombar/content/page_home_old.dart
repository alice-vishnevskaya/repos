import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:incomplete_app/incompletewidgets/EditText.dart';
import 'package:incomplete_app/incompletewidgets/IncompleteButton.dart';
//import 'package'

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
  //круги для людей рядом
  Widget bubble(double contextwidth, double size,{bool floppa = false}) => Container(
  child: SizedBox(
    width: contextwidth/5,
    height: contextwidth/5,
    child: Center(
      widthFactor: 10,
      child: floppa ? 
      CircleAvatar(
        //child: Image.asset('assets/floppa.png')
        radius: size/2,
        backgroundImage: NetworkImage('https://fretnoize.neocities.org/floppa.png')
      )
      :
      RawMaterialButton(
        shape: new CircleBorder(),
        fillColor: Color(0xffC4C4C4),
        onPressed: (){},
        child: Container(width: size,height: size,)
      )
    )
  )
  ); 



class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    List<Container> Bubbles;

    var bbls = Column(children: [
        bubble(width , width/9), bubble(width , width/9), bubble(width , width/9), bubble(width , width/9), bubble(width , width/9),
      ]
    );
    var bbls0 = Column(children: [
        bubble(width , width/9), bubble(width , width/7), bubble(width , width/7), bubble(width , width/9), bubble(width , width/9),
      ]
    );
    var bbls1 = Column(children: [
        bubble(width , width/9), bubble(width , width/7), bubble(width , width/5, floppa: true), bubble(width , width/7), bubble(width , width/9),
      ]
    ); 
      //Bubbles.add(Container(child: Row(children: bbls)));

    return Scaffold(
      backgroundColor: Color(0xffEBEBEB),
      body: Stack(children: [

        Container(
        margin: EdgeInsets.symmetric(vertical : width/8),
        child: Stack(
        children: [
            Container(
              width: width, height : height,
              child:  Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.symmetric(vertical : 25),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [//захардкодил круги
                    bbls,Container(child:bbls0,margin: EdgeInsets.symmetric(vertical: width/5/2)),bbls1,Container(child:bbls0,margin: EdgeInsets.symmetric(vertical: width/5/2)),bbls
                  ]
                ),
              )
            ),
          //bubble(width,width/15),

          

          Column(
            children: [
              Stack(
                children:[

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0x2aEBEBEB),width: 70),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0x3aEBEBEB),width: 65),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0x3aEBEBEB),width: 60),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0x3aEBEBEB),width: 55),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0x3aEBEBEB),width: 40),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0x3aEBEBEB),width: 35),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 90, sigmaY: 90),
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0x3aEBEBEB),width: 30),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 70, sigmaY: 70),
                    ),
                  ),
                  
                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0xaaEBEBEB),width: 25),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(140.0),
                        bottomRight: const Radius.circular(140.0),
                        topLeft: const Radius.circular(140.0),
                        topRight: const Radius.circular(140.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0xffEBEBEB),width: 25),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(120.0),
                        bottomRight: const Radius.circular(120.0),
                        topLeft: const Radius.circular(120.0),
                        topRight: const Radius.circular(120.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0xffEBEBEB),width: 25),
                      borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(80.0),
                        bottomRight: const Radius.circular(80.0),
                        topLeft: const Radius.circular(80.0),
                        topRight: const Radius.circular(80.0),
                      )
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(horizontal : 10 , vertical : 25),
                    decoration: BoxDecoration(
                      color: Color(0x00000000),
                      border: Border.all(color: Color(0xffEBEBEB),width: 25),
                    ),
                  ),

                  Container(
                    width: width,
                    height: width,
                    margin: EdgeInsets.symmetric(vertical : width),
                    decoration: BoxDecoration(
                      color: Color(0xffEBEBEB),
                    ),
                  ),
                  
                ]
              )
            ],
          ),

          

        ]
      )
    ),
    
    //кнопка для обновления
          Container(
            width: width,
            height: height,
          child: Container(
            //width: 200,
            margin: EdgeInsets.symmetric(horizontal : 20 , vertical : 30),
            alignment: Alignment.bottomRight,
            child: new RawMaterialButton(
              shape: new CircleBorder(
              ),
              fillColor: Color(0xff8E8CFE),
              //elevation: 50,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal : 10 , vertical :10),
              child: Icon(
                Icons.refresh,
                color: Colors.white,
                size: width/10,
              ),
              ),
            onPressed: (){},
            )
          ),
          ),

          Container(
            width: width,
            height: height,
            child: Container(
              
            ),
          ),

    Container(
          padding: EdgeInsets.only(top: width/4,left: width/4, right: width/4),
          //alignment: Alignment.topCenter,
          child: FlatButton(
            onPressed: (){},
            color: Color(0xff79B7F0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45.0),
              //side: BorderSide(color: Colors.red)
            ),
            //fillColor: Color(0xffBDDEB5),
            child: Container(
              child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 0, top: 13, bottom: 13, right: 2),
                  child: Icon(Icons.place_outlined, color: Colors.white),
                ),
                Container(
                  padding: EdgeInsets.only(right: 2),
                  child:
                Text('Milchakova 8A', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w500,fontSize: 20, color: Colors.white),)
                )
              ]
            )),
          )
        ),

        Container(
          padding: EdgeInsets.only(top: width*0.1,left: width/4, right: width/4),
          //alignment: Alignment.topCenter,
          
          child: FlatButton(
            onPressed: (){},
            color: Color(0xff79B7F0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
              //side: BorderSide(color: Colors.red)
            ),
            //fillColor: Color(0xffBDDEB5),
            child: Container(
              padding: EdgeInsets.only(bottom:width*0.14, top: width * 0.043),
              child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Container(
                //   padding: EdgeInsets.only(left: 0, top: 13, bottom: 13, right: 2),
                //   child: Icon(Icons.place_outlined, color: Colors.white),
                // ),
                Container(
                  padding: EdgeInsets.only(right: 2),
                  child:
                Text('People around', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w500,fontSize: 23, color: Colors.white),)
                )
              ]
            )),
          )
        ),

        Container(
          padding: EdgeInsets.only(top: width/4,left: width/4, right: width/4),
          //alignment: Alignment.topCenter,
          child: Container(
            decoration: BoxDecoration(boxShadow: [BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 7,
            blurRadius: 33,
            offset: Offset(0, 15),
            )]
            ),

            child: FlatButton(
            onPressed: (){},
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45.0),
              //side: BorderSide(color: Colors.red)
            ),
            //fillColor: Color(0xffBDDEB5),
            child: Container(
              child: Row(
              //mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 0, top: 13, bottom: 13, right: 2),
                  child: Icon(Icons.place_outlined, color: Colors.black),
                ),
                Container(
                  padding: EdgeInsets.only(right: 2),
                  child:
                Text('Milchakova 8A', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w500,fontSize: 20, color: Colors.black),)
                )
              ]
            )),
          ))
        ),

    ]
    )
    ); 
  }
}
