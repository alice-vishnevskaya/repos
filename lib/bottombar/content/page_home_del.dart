import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:isolate';

import 'package:background_locator/background_locator.dart';
import 'package:background_locator/location_dto.dart';
import 'package:background_locator/settings/android_settings.dart';
import 'package:background_locator/settings/ios_settings.dart';
import 'package:background_locator/settings/locator_settings.dart';
import 'package:location_permissions/location_permissions.dart';

import 'package:incomplete_app/locator/file_manager.dart';
import 'package:incomplete_app/locator/location_callback_handler.dart';
import 'package:incomplete_app/locator/location_service_repository.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
//круги для людей рядом
Widget bubble(double contextwidth, double size,{bool floppa = false}) => Container(
    child: SizedBox(
        width: contextwidth/5,
        height: contextwidth/5,
        child: Center(
            widthFactor: 10,
            child: floppa ?
            CircleAvatar(
              //child: Image.asset('assets/floppa.png')
                radius: size/2,
                backgroundImage: NetworkImage('https://fretnoize.neocities.org/floppa.png')
            )
                :
            RawMaterialButton(
                shape: new CircleBorder(),
                fillColor: Color(0xffC4C4C4),
                onPressed: (){},
                child: Container(width: size,height: size,)
            )
        )
    )
);



class _HomePageState extends State<HomePage> {

  ReceivePort port = ReceivePort();

  String logStr = '';
  bool isRunning;
  LocationDto lastLocation;
  DateTime lastTimeLocation;

  @override
  void initState() {
    super.initState();

    if (IsolateNameServer.lookupPortByName(
        LocationServiceRepository.isolateName) !=
        null) {
      IsolateNameServer.removePortNameMapping(
          LocationServiceRepository.isolateName);
    }

    IsolateNameServer.registerPortWithName(
        port.sendPort, LocationServiceRepository.isolateName);

    port.listen(
          (dynamic data) async {
        await updateUI(data);
      },
    );
    initPlatformState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> updateUI(LocationDto data) async {
    final log = await FileManager.readLogFile();

    await _updateNotificationText(data);

    setState(() {
      if (data != null) {
        lastLocation = data;
        lastTimeLocation = DateTime.now();
      }
      logStr = log;
    });
  }

  Future<void> _updateNotificationText(LocationDto data) async {
    if (data == null) {
      return;
    }

    await BackgroundLocator.updateNotificationText(
        title: "new location received",
        msg: "${DateTime.now()}",
        bigMsg: "${data.latitude}, ${data.longitude}");
  }

  Future<void> initPlatformState() async {
    print('Initializing...');
    await BackgroundLocator.initialize();
    logStr = await FileManager.readLogFile();
    print('Initialization done');
    final _isRunning = await BackgroundLocator.isServiceRunning();
    setState(() {
      isRunning = _isRunning;
    });
    print('Running ${isRunning.toString()}');
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery
        .of(context)
        .size
        .height;
    final width = MediaQuery
        .of(context)
        .size
        .width;

    List<Container> Bubbles;

    var bbls = Column(children: [
      bubble(width, width / 9),
      bubble(width, width / 9),
      bubble(width, width / 9),
      bubble(width, width / 9),
      bubble(width, width / 9),
    ]
    );
    var bbls0 = Column(children: [
      bubble(width, width / 9),
      bubble(width, width / 7),
      bubble(width, width / 7),
      bubble(width, width / 9),
      bubble(width, width / 9),
    ]
    );
    var bbls1 = Column(children: [
      bubble(width, width / 9),
      bubble(width, width / 7),
      bubble(width, width / 5, floppa: true),
      bubble(width, width / 7),
      bubble(width, width / 9),
    ]
    );

    //Bubbles.add(Container(child: Row(children: bbls)));

    return Scaffold(
        backgroundColor: Color(0xffEBEBEB),
        body: Stack(children: [

          Container(
              margin: EdgeInsets.symmetric(vertical: width / 8)
          ),

          //кнопка для обновления
          Container(
            width: width,
            height: height,
            child: Container(
              //width: 200,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                alignment: Alignment.bottomRight,
                child: new RawMaterialButton(
                  shape: new CircleBorder(
                  ),
                  fillColor: Color(0xff8E8CFE),
                  //elevation: 50,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Icon(
                      Icons.refresh,
                      color: Colors.white,
                      size: width / 10,
                    ),
                  ),
                  onPressed: () {
                    onStart();
                  },
                )
            ),
          ),

          Container(
            width: width,
            height: height,
            child: Container(

            ),
          ),

          Container(
              padding: EdgeInsets.only(
                  top: width / 4, left: width / 4, right: width / 4),
              //alignment: Alignment.topCenter,
              child: FlatButton(
                onPressed: () {},
                color: Color(0xff79B7F0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(45.0),
                  //side: BorderSide(color: Colors.red)
                ),
                //fillColor: Color(0xffBDDEB5),
                child: Container(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                left: 0, top: 13, bottom: 13, right: 2),
                            child: Icon(
                                Icons.place_outlined, color: Colors.white),
                          ),
                          Container(
                              padding: EdgeInsets.only(right: 2),
                              child:
                              Text('Milchakova 8A', textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.w500,
                                    fontSize: 20,
                                    color: Colors.white),)
                          )
                        ]
                    )),
              )
          ),

          Container(
              padding: EdgeInsets.only(
                  top: width * 0.1, left: width / 4, right: width / 4),
              //alignment: Alignment.topCenter,

              child: FlatButton(
                onPressed: () {},
                color: Color(0xff79B7F0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  //side: BorderSide(color: Colors.red)
                ),
                //fillColor: Color(0xffBDDEB5),
                child: Container(
                    padding: EdgeInsets.only(
                        bottom: width * 0.14, top: width * 0.043),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // Container(
                          //   padding: EdgeInsets.only(left: 0, top: 13, bottom: 13, right: 2),
                          //   child: Icon(Icons.place_outlined, color: Colors.white),
                          // ),
                          Container(
                              padding: EdgeInsets.only(right: 2),
                              child:
                              Text('People around', textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.w500,
                                    fontSize: 23,
                                    color: Colors.white),)
                          )
                        ]
                    )),
              )
          ),

          Container(
              padding: EdgeInsets.only(
                  top: width / 4, left: width / 4, right: width / 4),
              //alignment: Alignment.topCenter,
              child: Container(
                  decoration: BoxDecoration(boxShadow: [BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 7,
                    blurRadius: 33,
                    offset: Offset(0, 15),
                  )
                  ]
                  ),

                  child: FlatButton(
                    onPressed: () {},
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(45.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    child: Container(
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    left: 0, top: 13, bottom: 13, right: 2),
                                child: Icon(
                                    Icons.place_outlined, color: Colors.black),
                              ),
                              Container(
                                  padding: EdgeInsets.only(right: 2),
                                  child:
                                  Text('Milchakova 8A',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.black),)
                              )
                            ]
                        )),
                  ))
          ),


        ]
        )
    );
  }

  void onStop() async {
    BackgroundLocator.unRegisterLocationUpdate();
    final _isRunning = await BackgroundLocator.isServiceRunning();

    setState(() {
      isRunning = _isRunning;
//      lastTimeLocation = null;
//      lastLocation = null;
    });
  }

  void onStart() async {
    if (await _checkLocationPermission()) {
      _startLocator();
      final _isRunning = await BackgroundLocator.isServiceRunning();

      setState(() {
        isRunning = _isRunning;
        lastTimeLocation = null;
        lastLocation = null;
      });
    } else {
      // show error
    }
  }

  Future<bool> _checkLocationPermission() async {
    final access = await LocationPermissions().checkPermissionStatus();
    switch (access) {
      case PermissionStatus.unknown:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
        final permission = await LocationPermissions().requestPermissions(
          permissionLevel: LocationPermissionLevel.locationAlways,
        );
        if (permission == PermissionStatus.granted) {
          return true;
        } else {
          return false;
        }
        break;
      case PermissionStatus.granted:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  void _startLocator() {
    Map<String, dynamic> data = {'countInit': 1};
    BackgroundLocator.registerLocationUpdate(LocationCallbackHandler.callback,
        initCallback: LocationCallbackHandler.initCallback,
        initDataCallback: data,
/*
        Comment initDataCallback, so service not set init variable,
        variable stay with value of last run after unRegisterLocationUpdate
 */
        disposeCallback: LocationCallbackHandler.disposeCallback,
        iosSettings: IOSSettings(
            accuracy: LocationAccuracy.NAVIGATION, distanceFilter: 0),
        autoStop: false,
        androidSettings: AndroidSettings(
            accuracy: LocationAccuracy.NAVIGATION,
            interval: 5,
            distanceFilter: 0,
            client: LocationClient.google,
            androidNotificationSettings: AndroidNotificationSettings(
                notificationChannelName: 'Location tracking',
                notificationTitle: 'Start Location Tracking',
                notificationMsg: 'Track location in background',
                notificationBigMsg:
                'Background location is on to keep the app up-tp-date with your location. This is required for main features to work properly when the app is not running.',
                notificationIcon: '',
                notificationIconColor: Colors.grey,
                notificationTapCallback:
                LocationCallbackHandler.notificationCallback)));
  }
}