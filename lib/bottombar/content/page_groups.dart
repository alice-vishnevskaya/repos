//import 'package:shared/ui/placeholder/placeholder_image.dart';
//import 'dart:html';

import 'package:flutter/material.dart';

Widget Group(width) => Container(
  child: SizedBox(
    width: width*0.9,
    height: 120,
    child: Container(
      //widthFactor: 10,
      alignment: Alignment.centerLeft,
      child: RawMaterialButton(
        //shape: BoxBorder(),
        //fillColor: Color(0xffC4C4C4),
        onPressed: (){},
        child: Center(
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),color: Colors.white),
          child: Row(
            children: [
              Container(padding: EdgeInsets.only(left: 10),
                child: Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(color: Color(0xafC4C4C4)
                ),
                child: Center(
                  child: Container(
                    child: Icon(Icons.group, color: Colors.white, size: 40,),
                  ), 
                ),
              )),
            ],
          ),
          //alignment: Alignment.center,
          //padding: EdgeInsets.all(10),
          width: width*0.9,
          height: 100,
        )
        )
      )
    )
  )
  // height: 120,
  // child: Container(

  // decoration: BoxDecoration(
  //   borderRadius: BorderRadius.circular(10),
  //   color: Colors.white,
  // ),
  // )
); 

class GroupsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //bool isLandscape = MediaQuery.of(context).size.aspectRatio > 1;
    var columnCount = 1;

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      child: ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 23, bottom: width* 0.025,left : width* 0.05,right: width* 0.05),//left: width/4, right: width/4),
            //alignment: Alignment.topCenter,
            child: FlatButton(
              onPressed: (){},
              color: Color(0xff79B7F0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(45.0),
                //side: BorderSide(color: Colors.red)
              ),
              //fillColor: Color(0xffBDDEB5),
              child: Container(
                child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 0, top: 8, bottom: 10, right: 6),
                    child: Icon(Icons.group, color: Colors.white),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 2),
                    child:
                  Text('My groups', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w500,fontSize: 22, color: Colors.white),)
                  )
                ]
              )),
            )
          ),
          Group(width),
          Group(width),
          Group(width),
          Group(width),
          Group(width),
          Group(width),
          Group(width),
        ],
      )
    );
  }
}
