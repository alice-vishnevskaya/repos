

import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:isolate';

import 'package:background_locator/background_locator.dart';
import 'package:background_locator/location_dto.dart';
import 'package:background_locator/settings/android_settings.dart';
import 'package:background_locator/settings/ios_settings.dart';
import 'package:background_locator/settings/locator_settings.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
//import 'package:list_wheel_scroll_view_x/list_wheel_scroll_view_x.dart';
import 'package:location_permissions/location_permissions.dart';

import 'package:incomplete_app/locator/file_manager.dart';
import 'package:incomplete_app/locator/location_callback_handler.dart';
import 'package:incomplete_app/locator/location_service_repository.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:incomplete_app/auth_service.dart';

import 'package:carousel_slider/carousel_slider.dart';

import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

List<UserInfo> nearestUsers = [];
 class Wrapper extends StatefulWidget {
   @override
   _WrapperState createState() => _WrapperState();
 }
 
 class _WrapperState extends State<Wrapper> {
   @override
   Widget build(BuildContext context) {
    return  StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('users')
              .snapshots(),
          builder: (ctx, streamSnapshot) {

            if(streamSnapshot == null)
              
              return Center(child: CircularProgressIndicator(),);

            final documents = streamSnapshot.data.docs;
            documents.forEach((element){
                nearestUsers.add(new UserInfo(element.data()['username'].toString(), 
                'assets/floppa.png', element.data()['twitter'].toString(), element.data()['instagram'].toString(),
                 element.data()['soundcloud'].toString()));

            });
            return HomePage();
          }); 
   }
 }

 
class HomePage extends StatefulWidget {

  
  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {

  int curChosenUser=0;



  /*
  ReceivePort port = ReceivePort();
  final user = FirebaseAuth.instance.currentUser;
 
  // rest of the code|  do stuff
  String logStr = '';
  bool isRunning;
  LocationDto lastLocation;
  DateTime lastTimeLocation;

  */

  @override
  void initState() {
    super.initState();
   
    /*
    if (IsolateNameServer.lookupPortByName(
        LocationServiceRepository.isolateName) !=
        null) {
      IsolateNameServer.removePortNameMapping(
          LocationServiceRepository.isolateName);
    }

    IsolateNameServer.registerPortWithName(
        port.sendPort, LocationServiceRepository.isolateName);

    port.listen(
          (dynamic data) async {
        await updateUI(data);
      },
    );
    initPlatformState();
    */
  }

  @override
  void dispose() {
    super.dispose();
  }

  /*
  Future<void> updateUI(LocationDto data) async {
    final log = await FileManager.readLogFile();

    await _updateNotificationText(data);

    setState(() {
      if (data != null) {
        lastLocation = data;
        lastTimeLocation = DateTime.now();
      }
      logStr = log;
    });
  }

  Future<void> _updateNotificationText(LocationDto data) async {
    if (data == null) {
      return;
    
        
    }

    await BackgroundLocator.updateNotificationText(
        title: "new location received",
        msg: "${DateTime.now()}",
        bigMsg: "${data.latitude}, ${data.longitude}");
        //TODO:
        FirebaseFirestore.instance
              .collection('Locations/' ).doc('curr_pos').update({user.uid:[data.latitude, data.longitude]});
              print("send"+ data.latitude.toString() + " " + data.longitude.toString());
  }

  Future<void> initPlatformState() async {
    print('Initializing...');
    await BackgroundLocator.initialize();
    logStr = await FileManager.readLogFile();
    print('Initialization done');
    final _isRunning = await BackgroundLocator.isServiceRunning();
    setState(() {
      isRunning = _isRunning;
    });
    print('Running ${isRunning.toString()}');
  }
  */

  @override
  Widget build(BuildContext context) {
    //onStart();
    final height = MediaQuery
        .of(context)
        .size
        .height;
    final width = MediaQuery
        .of(context)
        .size
        .width;

    return Scaffold(
        backgroundColor: Color(0xffEBEBEB),
        body: Stack(children: [

          Container(
              margin: EdgeInsets.symmetric(vertical: width / 8)
          ),

          

          Container(
            width: width,
            height: height,
            child: Container(

            ),
          ),

          
          Container(
           //alignment: Alignment.center,
           padding: EdgeInsets.only(top:100),
           //margin: EdgeInsets.symmetric(vertical: 100),
           child: CarouselSlider(
            //itemExtent: 220,
            //diameterRatio: 3.5,
            //offAxisFraction: 0,
            options: CarouselOptions(
            //physics: FixedExtentScrollPhysics(),
             //useMagnifier: true,
             //magnification: 1.2,
             height:200,
             scrollDirection: Axis.horizontal,
             onPageChanged: (index, reason)=> {
               setState(() {
                 curChosenUser = index;
                 print(nearestUsers[curChosenUser].name);
               })
             },
            ),
            items: buildUserIcons(nearestUsers)
            ),
          ),

          Container(
              padding: EdgeInsets.only(top: 23),
              //margin: EdgeInsets.symmetric(horizontal: 20),
              //alignment: Alignment.topCenter,

              
                //fillColor: Color(0xffBDDEB5),
                
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisSize: MainAxisSize.min,
                  children: [Container(
                    padding: EdgeInsets.only(bottom: 50, top: 12,left: 14, right: 14),
                    decoration: BoxDecoration(
                      color: Color(0xff79B7F0),
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 7,
                    blurRadius: 28,
                    offset: Offset(0, 15),
                  )
                  ]
                    ),
                    child: Row(
                        mainAxisSize: MainAxisSize.min,
                        
                        children: [
                          // Container(
                          //   padding: EdgeInsets.only(left: 0, top: 13, bottom: 13, right: 2),
                          //   child: Icon(Icons.place_outlined, color: Colors.white),
                          // ),
                          Container(
                              //padding: EdgeInsets.only(right: 2),
                              child:
                              Text('People around', textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.w500,
                                    fontSize: 23,
                                    color: Colors.white),)
                          )
                        ]
                    ))]),
              
          ),

          Container(
              padding: EdgeInsets.only(top: 66),
              //alignment: Alignment.topCenter,
              child: Container(
                  decoration: BoxDecoration(
                  ),

                  child: Row( mainAxisAlignment: MainAxisAlignment.center,
                  children: [Container(
                    
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(45.0),
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    //   
                        child: Row(

                          mainAxisSize: MainAxisSize.min,
                            children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 13, bottom: 13, right: 2, left: 13),
                                    child: Icon(
                                        Icons.place_outlined, color: Colors.black),
                                  ),
                                  Container( padding: EdgeInsets.only(right:17),
                                  child: Text('Your location here',
                                    //textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.black),))
                                  
                            ]
                        ),
                  )]))
          ),
         
         //микро профиль с инфо
         Stack (children: [
           Container(
             padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column( children: [
          Row(
           //mainAxisSize: MainAxisSize.max,
           mainAxisAlignment: MainAxisAlignment.center,
           mainAxisSize: MainAxisSize.max,
         children: [
          Container(
          //height:160,
          //width: 150,
          margin: EdgeInsets.only(top: 305),

          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
                                  BoxShadow(
                                   color: Colors.grey.withOpacity(0.5),
                                   spreadRadius: -5,
                                    blurRadius: 30,
                                    offset: Offset(0, 6), // Позиция
                                   ),
                                  ],
          ),
          child: Container( margin: EdgeInsets.symmetric(horizontal: 3,vertical: 5),
            child: Column(children: [
            Container(margin: EdgeInsets.only(top:25,left:17,right:17,bottom:13),
            child: Text(
              //widget.nearestUsers[curChosenUser].name,
              "User info goes here",
              style: TextStyle(fontSize: 20),
              )
            ),

            //аккаунты ниже
            
            Container(
                    margin: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Color(0xFF5EA9DD),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    //   
                        child: Row(

                            children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 4, bottom: 4, right: 2, left: 7),
                                    child: Icon(
                                        MdiIcons.twitter, color: Colors.white),
                                  ),
                                  Container( padding: EdgeInsets.only(right:9),
                                  child: Text(nearestUsers[curChosenUser].twitter,
                                    //textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.white),))
                                  
                            ]
                        ),
                  ),
            Container(
                    margin: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Color(0xFFC13584),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    //   
                        child: Row(

                            children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 4, bottom: 4, right: 2, left: 4),
                                    child: Icon(
                                        MdiIcons.instagram, color: Colors.white),
                                  ),
                                  Container( padding: EdgeInsets.only(right:9),
                                  child: Text(nearestUsers[curChosenUser].instagram,
                                    //textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.white),))
                                  
                            ]
                        ),
                  ),
                Container(
                    margin: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Color(0xFFFE5000),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    //   
                        child: Row(

                            children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 4, bottom: 4, right: 5, left: 7),
                                    child: Icon(
                                        MdiIcons.soundcloud, color: Colors.white),
                                  ),
                                  Container( padding: EdgeInsets.only(right:9),
                                  child: Text(nearestUsers[curChosenUser].soundcloud,
                                    //textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.white),))
                                  
                            ]
                        ),
                  )
              
          ]))
          ),
         ]
         ),
         
         //данные какого то аккаунта
         
         ]
          )
         ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
             
            children: [
          Container(
          height:40,
          //width: 150,
          margin: EdgeInsets.only(top: 285),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 28,
                    offset: Offset(0, 8),
                  )
            ]
          ),
          child: Column(children: [
            Container(margin: EdgeInsets.symmetric(horizontal:15,vertical:10),
            child: Text(nearestUsers[curChosenUser].name,style: TextStyle(
              fontSize: 20,
            ),)),
          ],)
        ),
        
        ])
        ])
        ,

        //кнопка для обновления
          Container(
            width: width,
            height: height,
            child: Container(
              //width: 200,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                alignment: Alignment.bottomRight,
                child: new RawMaterialButton(
                  shape: new CircleBorder(
                  ),
                  fillColor: Color(0xff8E8CFE),
                  //elevation: 50,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Icon(
                      Icons.refresh,
                      color: Colors.white,
                      size: width / 10,
                    ),
                  ),
                  onPressed: () {
                    
                  },
                )
            ),
          ),

        ]
        )
    );
  }

  List<Widget> buildUserIcons(List<UserInfo> users)
  {
        List<Widget> list = [];

         

         for(int i=0;i<users.length;i++)
           {
             print(users[i].getIcon());
             list.add(ClipRRect(
             borderRadius: BorderRadius.circular(15),
             child: Image(image: AssetImage(users[i].icon),),
             ),

             );
           }
         return list;
  }


  
/*
  void onStop() async {
    BackgroundLocator.unRegisterLocationUpdate();
    final _isRunning = await BackgroundLocator.isServiceRunning();

    setState(() {
      isRunning = _isRunning;
//      lastTimeLocation = null;
//      lastLocation = null;
    });
  }

  void onStart() async {
    if (await _checkLocationPermission()) {
      _startLocator();
      final _isRunning = await BackgroundLocator.isServiceRunning();

      setState(() {
        isRunning = _isRunning;
        lastTimeLocation = null;
        lastLocation = null;
      });
    } else {
      // show error
    }
  }

  Future<bool> _checkLocationPermission() async {
    final access = await LocationPermissions().checkPermissionStatus();
    switch (access) {
      case PermissionStatus.unknown:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
        final permission = await LocationPermissions().requestPermissions(
          permissionLevel: LocationPermissionLevel.locationAlways,
        );
        if (permission == PermissionStatus.granted) {
          return true;
        } else {
          return false;
        }
        break;
      case PermissionStatus.granted:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  void _startLocator() {
    Map<String, dynamic> data = {'countInit': 1};
    BackgroundLocator.registerLocationUpdate(LocationCallbackHandler.callback,
        initCallback: LocationCallbackHandler.initCallback,
        initDataCallback: data,
/*
        Comment initDataCallback, so service not set init variable,
        variable stay with value of last run after unRegisterLocationUpdate
 */
        disposeCallback: LocationCallbackHandler.disposeCallback,
        iosSettings: IOSSettings(
            accuracy: LocationAccuracy.NAVIGATION, distanceFilter: 0),
        autoStop: false,
        androidSettings: AndroidSettings(
            accuracy: LocationAccuracy.NAVIGATION,
            interval: 5,
            distanceFilter: 0,
            client: LocationClient.google,
            androidNotificationSettings: AndroidNotificationSettings(
                notificationChannelName: 'Location tracking',
                notificationTitle: 'Start Location Tracking',
                notificationMsg: 'Track location in background',
                notificationBigMsg:
                'Background location is on to keep the app up-tp-date with your location. This is required for main features to work properly when the app is not running.',
                notificationIcon: '',
                notificationIconColor: Colors.grey,
                notificationTapCallback:
                LocationCallbackHandler.notificationCallback)));
  }
  */
}

class UserInfo
{
  String name = "null";
  String icon = "null";
  String twitter = "null";
  String instagram = "null";
  String soundcloud = "null";
  UserInfo(String name,String img, String twitter, String instagram, String soundcloud)
  {
         this.name = name;
         icon = img;
         this.twitter = twitter;
         this.instagram = instagram;
         this.soundcloud = soundcloud;
  }

  String getIcon()
  {
     return icon;
  }
}