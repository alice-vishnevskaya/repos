//import 'package:shared/ui/placeholder/placeholder_card_short.dart';
//import 'dart:html';
//import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:incomplete_app/auth_service.dart';
import 'package:incomplete_app/settings/screen_settings.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:incomplete_app/auth_service.dart';

import 'package:background_locator/settings/locator_settings.dart';
import 'package:background_locator/location_dto.dart';
import 'package:background_locator/background_locator.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:incomplete_app/locator/location_callback_handler.dart';
import 'package:incomplete_app/locator/location_service_repository.dart';

import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Profilepage extends StatefulWidget {
  final VoidCallback callbackFunc;
  Profilepage(this.callbackFunc);
  @override
  _ProfilepageState createState() => _ProfilepageState(callbackFunc);
}

class _ProfilepageState extends State<Profilepage> {
  VoidCallback callbackStopTracking;
  _ProfilepageState(this.callbackStopTracking);
  void openSettings(BuildContext context)
  {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));
  }

   @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Stack(
      fit: StackFit.expand,
      children: [
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(255, 255, 255, 1),
                Color.fromRGBO(216, 216, 216, 1),
              ],
              begin: FractionalOffset.bottomCenter,
              end: FractionalOffset.topCenter,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.settings,
                          color: Colors.white,
                        ),
                            onPressed: (){
                          openSettings(context);
                        }
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.logout,
                          color: Colors.white,
                        ),
                            onPressed: () {
                              context.read<AuthenticationService>().signOut();
                              callbackStopTracking();
                        }
                      ),
                    ],
                  ),

                  Container(
                    height: height*0.8,
                    child: LayoutBuilder(
                      builder: (context, constraints) {
                        double innerHeight = constraints.maxHeight;
                        double innerWidth = constraints.maxWidth;
                        return Stack(
                          fit: StackFit.expand,

                          children: [
                              Positioned(
                              //top: 85,
                              //bottom: 0,
                              left: 0,
                              right: 0,
                              child: Container(
                                
                                width: innerWidth,
                                //margin: EdgeInsets.only(bottom: 0 ),
                                margin: EdgeInsets.only(top:85),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [
                                  BoxShadow(
                                   color: Colors.grey.withOpacity(0.5),
                                   spreadRadius: -5,
                                    blurRadius: 30,
                                    offset: Offset(0, 6), // Позиция
                                   ),
                                  ],
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    SizedBox(
                                      height: 80,
                                    ),
                                    Text(
                                      'Untitled User',
                                      style: TextStyle(
                                        //color: Color.fromRGBO(0, 0, 0, 1),
                                        //fontFamily: 'Nunito',
                                        fontSize: 30,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),

                                    Column(
                                      children: [
                                        Text('id0000',style: TextStyle(fontSize: 17)),
                                        Text('City, Country',style: TextStyle(fontSize: 17)),
                                      ],
                                    ),

                                    SizedBox(height: 10,),

                                    Container( //рифат писал 
                                      //width: innerWidth * 0.9,
                                      margin: EdgeInsets.all(12),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7),
                                        color: Color(0xfff3f3f4)
                                        ),
                                      child:
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            Text(
                                              'Friends',
                                              style: TextStyle(
                                                color: Colors.grey[700],
                                                //fontFamily: 'Nunito',
                                                fontSize: 17,
                                              ),
                                            ),
                                            Text(
                                              '0',
                                              style: TextStyle(
                                                color: Color.fromRGBO(
                                                    39, 105, 171, 1),
                                                //fontFamily: 'Nunito',
                                                fontSize: 25,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 25,
                                            vertical: 8,
                                          ),
                                          child: Container(
                                            height: 50,
                                            width: 3,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              'Requests',
                                              style: TextStyle(
                                                color: Colors.grey[700],
                                                //fontFamily: 'Nunito',
                                                fontSize: 17,
                                              ),
                                            ),
                                            Text(
                                              '0',
                                              style: TextStyle(
                                                color: Color.fromRGBO(
                                                    39, 105, 171, 1),
                                                //fontFamily: 'Nunito',
                                                fontSize: 25,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    ),

                  Row(
                    
                    children:[
                  Container(
                    margin: EdgeInsets.only(right:12,left:12,top:4,bottom:10),
                    decoration: BoxDecoration(
                      color: Color(0xFF5EA9DD),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    //   
                        child: Row(
                            mainAxisSize: MainAxisSize.min,
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 4, bottom: 4, right: 2, left: 7),
                                    child: Icon(
                                        MdiIcons.twitter, color: Colors.white),
                                  ),
                                  Container( padding: EdgeInsets.only(right:8),
                                  child: Text('@twitterexample',
                                    //textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.white),))
                                  
                            ]
                        ),
                  )]),
        
                                  ],
                                ),
                              ),
                            ),
                            

                            Positioned(
                              top: 0,
                              left: 0,
                              right: 0,

                              child: Center(

                                child: Container(

                                  child: CircleAvatar(
                                    backgroundImage: NetworkImage(''),
                                  radius: 79.0,
                                  backgroundColor: Color(0xffd2d2d2),
                                  ),
                                ),
                              ),

                            ),

                          ],
                        );
                      },
                    ),
                  ),

                  
                ],
              ),
            ),
          ),
        )
        ),
      ],
    );
  }
}
