import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class AuthenticationService {

  final FirebaseAuth _firebaseAuth;

  AuthenticationService(this._firebaseAuth);

  Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<String> signIn(String email, String password) async{
    try {
      await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
     
      return "You are now signed in";
    } on FirebaseAuthException catch(e){
      return e.message;
    }
  }

  Future<String> signUp(String email, String password) async{
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
      
      FirebaseFirestore.instance.collection('users' ).
      doc(_firebaseAuth.currentUser.uid).set({
        'Location':[8.642, 3.445544545],
        'email':email, 
        'username':'no name',
        'twitter':'@pen',
        'vk':'https://vk.com/bober'
        });


      return "You are now signed up";
    } on FirebaseAuthException catch(e){
      return e.message;
    }
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }
}